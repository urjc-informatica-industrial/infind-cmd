#!/bin/bash

DIR=~/tools/infind-cmd

if [ ! -d "$DIR" ]; then
  rm -rf ~/.nvm
  echo "Instalando nvm"
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
  source ~/.bashrc
  nvm install v18.14.1
  nvm alias default v18.14.1
  nvm use default

  echo "Descargando comando"
  mkdir -p ~/tools
  cd ~/tools
  git clone https://gitlab.com/urjc-informatica-industrial/infind-cmd
fi

echo "Actualizando comando"
cd $DIR
git pull
npm i
node infind.mjs init
echo "Instalación finalizada"
