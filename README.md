# InfInd CMD

URJC Informática Industrial command.

## How to install

```sh
curl -sSf https://gitlab.com/urjc-informatica-industrial/infind-cmd/-/raw/main/install.sh | bash
```