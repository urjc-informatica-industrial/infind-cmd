// @ts-check
import { program } from 'commander';
import fs from 'fs';
import os from 'os';
import path from 'path';
import ini from 'ini';
import inquirer from 'inquirer';
import exec from 'node-async-exec';
import sqlite3 from 'sqlite3';
sqlite3.verbose();

const __dirname = path.dirname(new URL(import.meta.url).pathname);
const executable = path.resolve(__dirname, 'infind.mjs')
const homedir = os.homedir();
const configFilePath = path.join(homedir, '.infind.ini');


let config = {};

try{
    config = ini.parse(fs.readFileSync(configFilePath, 'utf-8'));
} catch(e) {
    console.log("No existe configuración de infind.")
}

function updateConfig(newConfig) {
    config = newConfig;
    fs.writeFileSync(configFilePath, ini.stringify(config));
}

program
  .name('infind')
  .description('CLI de Informática Industrial')
  .version('1.3.9');

program
    .command('update')
    .description('Intenta actualizar el comando a la última versión disponible.')
    .action(async () => {   
        console.log("Comprobando actualizaciones del comando")
        const command = `cd ${__dirname}; git pull; npm i`
        await exec({ cmd: `bash -c "${command}"`})
        console.log("Listo.")
    });

program
    .command('init')
    .description('Instala el comando y inicializa el entorno por defecto.')
    .action(async () => {  
        const fileExist = fs.existsSync(configFilePath);
        // Comprobar si el archivo de configuración ya existe
        if (fileExist) {
            console.log('El archivo de configuración ya existe.');
        }

        // Preguntar al usuario si desea proporcionar un ID de usuario
        const answers = await inquirer.prompt([
            {
                type: 'confirm',
                name: 'changeDomainID',
                message: '¿Desea cambiar un ID de Dominio del usuario?',
                when: function(answers) {
                    return fileExist;
                },
                default: true
            },
            {
                type: 'input',
                name: 'ROS_DOMAIN_ID',
                message: 'Ingrese el ID de usuario:',
                when: function(answers) {
                    return answers.changeDomainID;
                },
                validate: function(value) {
                    if (value.trim().length === 0) {
                    return 'Por favor, ingrese un ID de usuario válido.';
                    }
                    return true;
                }
            }
        ]);

        // Crear un archivo de configuración
        const defaultDomain = config.ROS_DOMAIN_ID ? config.ROS_DOMAIN_ID : 100;
        // console.log(answers)
        updateConfig({
            ...config,
            ROS_DOMAIN_ID: answers.ROS_DOMAIN_ID ? answers.ROS_DOMAIN_ID : defaultDomain
        });
        console.log('El archivo de configuración se ha actualizado.');

        await setEnvironment(config.ROS_ENV);        
    });

function wrapCommonConfiguration(customConfiguration) {
    return `source /opt/ros/humble/setup.bash
    export ROS_VERSION=2
    export ROS_PYTHON_VERSION=3
    export ROS_DISTRO=humble    
    export ROS_LOCALHOST_ONLY=1    
    export PATH=~/.local/bin:$PATH
    ${customConfiguration}        
    source $PROJECT_WS/.venv/bin/activate
    source $PROJECT_WS/install/setup.bash

    cd $PROJECT_WS
    alias p="cd $PROJECT_CODE"
    alias ws="cd $PROJECT_WS"
    alias infind="node ${executable}"
    `.split('\n').map((line)=>line.trimStart()).join('\n');
}

async function setWorkSpace1() {
    const infind_bashrc = path.join(homedir, '.bashrc_infind');

    const fileContent = wrapCommonConfiguration(`
    export PROJECT_WS=~/ros2_ws
    export PROJECT_CODE=$PROJECT_WS/src/project1
    export ROS_DOMAIN_ID=${config.ROS_DOMAIN_ID}
    export PS1="${ '($ROS_DOMAIN_ID:🌧️ P1) ${debian_chroot:+($debian_chroot)}\\u@\\h:\\w\\$ ' }"
    `);

    fs.writeFileSync(infind_bashrc, fileContent);

    await execIfMissing(
        'ros2_ws',
        '- Creando carpeta de workspace para proyecto 1.',
        `mkdir -p ~/ros2_ws/src`
    )

    await execIfMissing(
        'ros2_ws/.venv',
        '- Iniciar entorno de python en ros2_ws',
        `cd ~/ros2_ws
        python3 -m venv .venv
        source ~/ros2_ws/.venv/bin/activate
        python3 -m pip install colcon-common-extensions empy lark ttkbootstrap`
    )

    await execIfMissing(
        'ros2_ws/build',
        '- Ejecutando colcon build en carpeta de proyecto.',
        `source /opt/ros/humble/setup.bash;
        cd ~/ros2_ws
        colcon build`
    )

    await execIfMissing(
        'ros2_ws/src/weather-station',
        '- Descargando librería de weather_station de dependencia del proyecto.',
        `source /opt/ros/humble/setup.bash;
        cd ~/ros2_ws/src
        git clone https://gitlab.com/urjc-informatica-industrial/weather-station.git
        cd ~/ros2_ws
        colcon build --packages-select weather_station_msgs weather_station_ui simulated_sensors_board_library
        `
    )
}

async function execIfMissing(check_folder, message, command) {
    if (!fs.existsSync(path.join(homedir, check_folder))){
        console.log(message)
        try {
            await exec({ cmd: `bash -c "${command}"`})
        } catch(e) {
            console.error(e);
        }
    }
}

async function execAlways(message, command) {    
    console.log(message)
    try {
        await exec({ cmd: `bash -c "${command}"`})
    } catch(e) {
        console.error(e);
    }
}

async function setWorkSpace2() {
    const infind_bashrc = path.join(homedir, '.bashrc_infind');
    const dir = path.join(homedir, 'project2_ws');
    const dir_code = path.join(homedir, 'project2_ws/src/project2');

    const fileContent = wrapCommonConfiguration(`
    export PROJECT_WS=~/project2_ws
    export PROJECT_CODE=$PROJECT_WS/src/project2
    export TURTLEBOT3_MODEL=burger
    export ROS_DOMAIN_ID=${config.ROS_DOMAIN_ID}
    export PS1="${ '($ROS_DOMAIN_ID:🐢P2) ${debian_chroot:+($debian_chroot)}\\u@\\h:\\w\\$ ' }"
    alias maze="killall -9 gzserver; killall -9 gzclient; (ros2 launch infind_maze_resources infind_maze.launch.py &); sleep 300; killall -9 gzserver; killall -9 gzclient"
    alias stop_maze="killall -9 gzserver; killall -9 gzclient"
    alias maze_home="killall -9 gzserver; killall -9 gzclient; (ros2 launch infind_maze_resources infind_maze.launch.py &); sleep 900; killall -9 gzserver; killall -9 gzclient"
    `);

    await execIfMissing(
        'project2_ws',
        '- Creando carpeta de workspace para proyecto 2.',
        `mkdir -p ~/project2_ws/src`
    )

    await execIfMissing(
        'project2_ws/.venv',
        '- Iniciar entorno de python en project2_ws',
        `cd ~/project2_ws
        python3 -m venv .venv
        source ~/project2_ws/.venv/bin/activate
        python3 -m pip install colcon-common-extensions empy lark ttkbootstrap lxml numpy`
    )

    await execIfMissing(
        'project2_ws/build',
        '- Ejecutando colcon build en carpeta de proyecto.',
        `source /opt/ros/humble/setup.bash;
        cd ~/project2_ws
        colcon build`
    )

    await execIfMissing(
        'project2_ws/src/turtlebot3_simulations',
        '- Cargando entorno de turtlebot3_simulations.',
        `source ~/project2_ws/install/setup.bash
        cd ~/project2_ws/src
        git clone -b humble-devel https://github.com/ROBOTIS-GIT/DynamixelSDK.git
        git clone -b humble-devel https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
        git clone -b humble-devel https://github.com/ROBOTIS-GIT/turtlebot3.git
        git clone -b humble-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
        cd ~/project2_ws
        colcon build --symlink-install
        `
    )    

    await execIfMissing(
        'project2_ws/src/mazes_infind',
        '- Cargando laberintos de la práctica.',
        `source ~/project2_ws/install/setup.bash
        cd ~/project2_ws
        git clone https://gitlab.com/urjc-informatica-industrial/mazes_infind.git src/mazes_infind
        colcon build --packages-select infind_maze_resources maze_collision_detector
        `
    )    

    fs.writeFileSync(infind_bashrc, fileContent);
}

async function setWorkSpace3() {
    const infind_bashrc = path.join(homedir, '.bashrc_infind');
    const dir = path.join(homedir, 'project3_ws');
    const dir_code = path.join(homedir, 'project3_ws/src/project3');

    const fileContent = wrapCommonConfiguration(`
    export DB_CLIENT=~/tools/dbeaver/dbeaver    
    export PROJECT_WS=~/project3_ws
    export PROJECT_CODE=$PROJECT_WS/src/project3
    export TURTLEBOT3_MODEL=waffle_pi
    export ROS_DOMAIN_ID=${config.ROS_DOMAIN_ID}
    export DB_FILE=$PROJECT_WS/db/db1.db
    export PS1="${ '($ROS_DOMAIN_ID:📦P3) ${debian_chroot:+($debian_chroot)}\\u@\\h:\\w\\$ ' }"
    alias initial_pose="ros2 topic pub -1 /initialpose geometry_msgs/PoseWithCovarianceStamped '{ header: {stamp: {sec: 0, nanosec: 0}, frame_id: "map"}, pose: { pose: {position: {x: 0.1, y: 0.0, z: 0.0}, orientation: {w: 1}}, } }'"
    alias stop_store="killall -9 gzserver; killall -9 gzclient; killall -9 component_container_isolated; killall -9 rviz2"
    alias store="stop_store; (ros2 launch infind_store_resources infind_store.launch.py &); (ros2 launch infind_store_resources infind_nav2.launch.py &); (sleep 3 && initial_pose &); sleep 600; stop_store"    
    alias store_home="stop_store; (ros2 launch infind_store_resources infind_store.launch.py &); (ros2 launch infind_store_resources infind_nav2.launch.py &); (sleep 3 && initial_pose &); sleep 1200; stop_store"
    alias db="$DB_CLIENT -con 'driver=sqlite|database=$DB_FILE|name=Project3DB'"
    `);

    await execIfMissing(
        'project3_ws',
        '- Creando carpeta de workspace para proyecto 3.',
        `mkdir -p ~/project3_ws/src`
    )

    await execIfMissing(
        'project3_ws/.venv',
        '- Iniciar entorno de python en project3_ws',
        `cd ~/project3_ws
        python3 -m venv .venv
        source ~/project3_ws/.venv/bin/activate
        python3 -m pip install colcon-common-extensions empy lark ttkbootstrap tksheet lxml numpy`
    )

    await execIfMissing(
        'project3_ws/build',
        '- Ejecutando colcon build en carpeta de proyecto.',
        `source /opt/ros/humble/setup.bash;
        cd ~/project3_ws
        colcon build`
    )

    await execIfMissing(
        'project3_ws/src/turtlebot3_simulations',
        '- Cargando entorno de turtlebot3_simulations.',
        `source ~/project3_ws/install/setup.bash
        cd ~/project3_ws/src
        ln -s ~/project2_ws/src/DynamixelSDK .
        ln -s ~/project2_ws/src/turtlebot3_msgs .
        ln -s ~/project2_ws/src/turtlebot3 .
        ln -s ~/project2_ws/src/turtlebot3_simulations .
        cd ~/project3_ws
        colcon build --symlink-install
        `
    )    

    await execIfMissing(
        'project3_ws/src/turtlebot3_simulations',
        '- Cargando entorno de turtlebot3_simulations.',
        `source ~/project3_ws/install/setup.bash
        cd ~/project3_ws/src
        ln -s ~/project2_ws/src/DynamixelSDK .
        ln -s ~/project2_ws/src/turtlebot3_msgs .
        ln -s ~/project2_ws/src/turtlebot3 .
        ln -s ~/project2_ws/src/turtlebot3_simulations .
        cd ~/project3_ws
        colcon build --symlink-install
        `
    ) 

    await execIfMissing(
        'project3_ws/src/infind_store',
        '- Cargando recursos de infind de práctica 3.',
        `
        cd ~/project3_ws/src
        git clone https://gitlab.com/urjc-informatica-industrial/infind-store.git infind_store
        cd ~/project3_ws
        colcon build --packages-select infind_store_resources infind_store_msgs infind_shelves_controller
        `
    )

    await execIfMissing(
        'project3_ws/src/infind_store/infind_store_msgs',
        '- Actualizando recursos práctica 3',
        `
        cd ~/project3_ws/src/infind_store
        git pull 
        cd ~/project3_ws
        colcon build --packages-select infind_store_resources infind_store_msgs
        `
    )

    await execIfMissing(        
        'project3_ws/src/infind_store/infind_store_msg/msgs',
        '- Actualizando recursos práctica 3',
        `
        cd ~/project3_ws/src/infind_store
        git pull 
        cd ~/project3_ws
        colcon build --packages-select infind_store_msgs
        `
    )

    await execIfMissing(        
        'project3_ws/src/infind_store/infind_shelves_controller',
        '- Actualizando utilidades de control',
        `
        cd ~/project3_ws/src/infind_store
        git pull 
        cd ~/project3_ws
        colcon build --packages-select infind_shelves_controller
        `
    )

    await execIfMissing(
        'tools/dbeaver',
        '- Cargando cliente de base de datos.',
        `cd ~/tools
        wget https://dbeaver.io/files/dbeaver-ce-latest-linux.gtk.x86_64-nojdk.tar.gz
        tar zxvf dbeaver-ce-latest-linux.gtk.x86_64-nojdk.tar.gz
        rm dbeaver-ce-latest-linux.gtk.x86_64-nojdk.tar.gz
        `
    )

    await execIfMissing(
        'project3_ws/db',
        '- Creando base de datos.',
        `cd ~/project3_ws
        mkdir db        
        `
    )

    await execIfMissing(
        'project3_ws/db',
        '- Creando base de datos.',
        `cd ~/project3_ws
        mkdir db        
        `
    )

    await execIfMissing(
        'project3_ws/.venv/lib/python3.10/site-packages/tksheet',
        '- Instalar tksheet',
        `cd ~/project3_ws
        source ~/project3_ws/.venv/bin/activate
        python3 -m pip install tksheet`
    )

    await execAlways(
        '- Revisar actualizaciones en dependencias del proyecto',
        `cd ~/project3_ws/src/infind_store
        git add .
        git reset --hard
        git pull 
        cd ~/project3_ws
        colcon build --packages-select infind_store_resources infind_store_msgs infind_shelves_controller`
    )

    fs.writeFileSync(infind_bashrc, fileContent);
}

async function setEnvironment(/** @type {string} */ environment) {        
    if (!environment) {
        environment="p1";
    }
    environment = environment.toLowerCase();
    if (!/^(p1|p2|p3)$/i.test(environment)) {
        console.log('Error: el entorno debe ser p1, p2 o p3');
        process.exit(1);
    }

    updateConfig({
        ...config,
        ROS_ENV: environment
    });

    switch (environment) {
        case 'p1':
            await setWorkSpace1();
            break;
        case 'p2':
            await setWorkSpace2();
            break;
        case 'p3':
            await setWorkSpace3();
            return;
    }

    console.log('Init project', environment);
    console.log('Cierra y abre la terminal o ejecuta el siguiente comando para aplicar los cambios.\n\nsource ~/.bashrc\n')
}

async function populateDatabase() {
    try{
        await exec({ cmd: `rm -rf ~/project3_ws/db`})
    }
    catch {
        console.log('Error deleting folder')
    }

    try {
        await exec({ cmd: `mkdir ~/project3_ws/db`})
    }
    catch {
        console.log('Error creating folder')
    }
    
    // Crear una nueva instancia de la base de datos en memoria
    let dbPath = path.join(homedir, 'project3_ws', 'db', 'db1.db')
    if (fs.existsSync(dbPath)) {
        fs.unlinkSync(dbPath); 
    }
    let db = new sqlite3.Database(dbPath);

    function dbAll(query, values=[]) {
        return new Promise((resolve,reject)=>{
            db.all(query, values,(err,rows)=>{
                if(err) reject(err)
                resolve(rows)
            })
        })
    }

    function dbRun(query, values=[]) {
        return new Promise((resolve,reject)=>{
            db.run(query, values, function (err) {
                if(err) reject(err)
                resolve(this)
            })
        })
    }

    // Crear la tabla PRODUCT
    await dbRun('CREATE TABLE PRODUCT (id INTEGER PRIMARY KEY, productCode TEXT, productName TEXT, price FLOAT)');

    // Crear una lista de 20 productos
    const products = [
        { productCode: 'P001', productName: 'Papel de impresora', price: 4.99 },
        { productCode: 'P002', productName: 'Lápices', price: 1.50 },
        { productCode: 'P003', productName: 'Bolígrafos', price: 1.25 },
        { productCode: 'P004', productName: 'Marcadores', price: 2.75 },
        { productCode: 'P005', productName: 'Tijeras', price: 3.99 },
        { productCode: 'P006', productName: 'Pegamento en barra', price: 1.75 },
        { productCode: 'P007', productName: 'Notas adhesivas', price: 0.99 },
        { productCode: 'P008', productName: 'Cinta adhesiva', price: 1.25 },
        { productCode: 'P009', productName: 'Grapas', price: 0.50 },
        { productCode: 'P010', productName: 'Perforadora', price: 8.99 },
        { productCode: 'P011', productName: 'Archivadores', price: 5.99 },
        { productCode: 'P012', productName: 'Carpeta con anillas', price: 3.50 },
        { productCode: 'P013', productName: 'Carpetas colgantes', price: 2.25 },
        { productCode: 'P014', productName: 'Calculadora', price: 12.99 },
        { productCode: 'P015', productName: 'Agenda', price: 9.99 },
        { productCode: 'P016', productName: 'Grapadora', price: 4.25 },
        { productCode: 'P017', productName: 'Cizalla', price: 19.99 },
        { productCode: 'P018', productName: 'Borrador', price: 0.99 },
        { productCode: 'P019', productName: 'Portaminas', price: 1.75 },
        { productCode: 'P020', productName: 'Rotulador', price: 1.50 },
    ];
    
    // Insertar los 20 productos en la tabla PRODUCT
    await Promise.all(products.map(async (product) => {
        await dbRun('INSERT INTO PRODUCT (productCode, productName, price) VALUES (?, ?, ?)', [product.productCode, product.productName, product.price]);
    }));

    // Obtener todos los productos de la tabla PRODUCT y mostrarlos por consola
    
    let rows = await dbAll('SELECT * FROM PRODUCT');
    rows.forEach(row => {
        console.log(`${row.id} | ${row.productCode} | ${row.productName} | ${row.price}`);
    });

    // Crear la tabla STORAGE_SHELF
    await dbRun('CREATE TABLE STORAGE_SHELF (id INTEGER PRIMARY KEY, shelfName TEXT, locationX FLOAT, locationY FLOAT)');
    await dbRun(`CREATE TABLE STORAGE_ITEM (
        storageShelfId INTEGER,
        productId INTEGER,
        quantity INTEGER,
        FOREIGN KEY (storageShelfId) REFERENCES STORAGE_SHELF(id),
        FOREIGN KEY (productId) REFERENCES PRODUCT(id),
        PRIMARY KEY (storageShelfId, productId)
      );`);

    // Crear una lista de 8 estanterías con coordenadas x e y
    const shelves = [
        { shelfName: 'Estantería 1', location_x: -1.881203, location_y: 5.053046 },
        { shelfName: 'Estantería 2', location_x: 1.680894, location_y: 5.097794 },
        { shelfName: 'Estantería 3', location_x: 4.099954, location_y: 1.724051 },
        { shelfName: 'Estantería 4', location_x: 4.259444, location_y: -1.885899 },
        { shelfName: 'Estantería 5', location_x: 1.982538, location_y: -4.978827 },
        { shelfName: 'Estantería 6', location_x: -1.636198, location_y: -5.043471 },
        { shelfName: 'Estantería 7', location_x: -4.301246, location_y: -1.346546 },
        { shelfName: 'Estantería 8', location_x: -4.189356, location_y: 2.01992 },
        { shelfName: 'Entregas', location_x: 0, location_y: 0 },
        { shelfName: 'Salida', location_x: -4.845454, location_y: -4.895001 },
    ];

    // Insertar las 8 estanterías en la tabla STORAGE_SHELF
    await Promise.all(shelves.map(async (shelf) => {
        await dbRun('INSERT INTO STORAGE_SHELF (shelfName, locationX, locationY) VALUES (?, ?, ?)', [shelf.shelfName, shelf.location_x, shelf.location_y]);
    }))

    // Obtener todas las estanterías de la tabla STORAGE_SHELF y mostrarlas por consola
    rows = await dbAll('SELECT * FROM STORAGE_SHELF', [])
    
    rows.forEach(row => {
        console.log(`${row.id} | ${row.shelfName} | ${row.location_x} | ${row.location_y}`);
    });

    // Crear la tabla ORDER
    await dbRun('CREATE TABLE DELIVERY_ORDER (orderNumber INTEGER PRIMARY KEY, deliveryAddress TEXT, createdAt DATETIME DEFAULT CURRENT_TIMESTAMP)');
    console.log('Created order')

    // Crear la tabla LINE_ITEM
    await dbRun('CREATE TABLE LINE_ITEM (orderNumber INTEGER, productId INTEGER, quantity INTEGER, FOREIGN KEY(orderNumber) REFERENCES DELIVERY_ORDER(orderNumber), FOREIGN KEY(productId) REFERENCES PRODUCT(id))');

    // Función auxiliar para generar una cantidad aleatoria de productos para un pedido
    function generateRandomItems() {
        const minItems = 1;
        const maxItems = 3;
        return Math.floor(Math.random() * (maxItems - minItems + 1) + minItems);
    }

    // Función auxiliar para generar un pedido aleatorio
    function generateRandomOrder() {
        
        return {
            deliveryAddress: `Dirección aleatoria ${Math.floor(Math.random() * 100)}`,
            /** @type {{productId: number, quantity: number}[]} */
            items: [], 
        };
    }

    // Obtener todos los productos de la tabla PRODUCT
    let productsDB = await dbAll('SELECT * FROM PRODUCT', []);
        
    // Generar 5 pedidos aleatorios
    const orders = Array.from({ length: 3 }, generateRandomOrder);

    // Para cada pedido, generar un número aleatorio de productos y asignar una cantidad aleatoria para cada uno
    orders.forEach(order => {
        const numItems = generateRandomItems();
        for (let i = 0; i < numItems; i++) {
            const product = productsDB[Math.floor(Math.random() * productsDB.length)];
            order.items.push({
                productId: product.id,
                quantity: Math.floor(Math.random() * 5) + 1, // Cantidad aleatoria entre 1 y 5
            });
        }
    });

    // Insertar los pedidos y los productos en la tabla LINE_ITEM
    await Promise.all(orders.map(async (order) => {
        let orderDB = await dbRun('INSERT INTO DELIVERY_ORDER (deliveryAddress) VALUES (?)', [order.deliveryAddress]);
        const orderNumber = orderDB.lastID; // Obtener el ID del pedido insertado
        await Promise.all(order.items.map(async (item) => {
            await dbRun('INSERT INTO LINE_ITEM (orderNumber, productId, quantity) VALUES (?, ?, ?)', [orderNumber, item.productId, item.quantity])
            console.log(`Pedido ${orderNumber} | Producto ${item.productId} | Cantidad ${item.quantity} insertado en la tabla LINE_ITEM`);
        }));
    }));

    // Obtener todos los pedidos y los productos de la tabla LINE_ITEM y mostrarlos por consola
    rows = await dbAll('SELECT DELIVERY_ORDER.orderNumber, DELIVERY_ORDER.deliveryAddress, LINE_ITEM.productId, LINE_ITEM.quantity FROM DELIVERY_ORDER INNER JOIN LINE_ITEM ON DELIVERY_ORDER.orderNumber = LINE_ITEM.orderNumber', []);    
    console.log('Pedidos:');
    console.log('Pedido | Dirección | Producto | Cantidad');
    rows.forEach(row => {
        console.log(`${row.orderNumber} | ${row.deliveryAddress} | ${row.productId} | ${row.quantity}`);
    });

    const shelvesDB = await dbAll('SELECT * FROM STORAGE_SHELF WHERE shelfName like "Estantería%"');
    // const productsDB = await dbAll("SELECT * FROM PRODUCT");

    for (const product of productsDB) {
        const numShelves = Math.floor(Math.random() * 2) + 1; // máximo 2 estanterías
        const selectedShelves = [];
        
        // seleccionar numShelves estanterías al azar
        while (selectedShelves.length < numShelves) {
            const randomIndex = Math.floor(Math.random() * shelvesDB.length);
            const selectedShelf = shelvesDB[randomIndex];
            if (!selectedShelves.includes(selectedShelf)) {
            selectedShelves.push(selectedShelf);
            }
        }
        
        // distribuir el producto en las estanterías seleccionadas
        const quantity = Math.floor(Math.random() * 10) + 1; // máximo 10 unidades
        for (const selectedShelf of selectedShelves) {
            const storageItem = {
            storageShelfId: selectedShelf.id,
            productId: product.id,
            quantity: quantity,
            };
            await dbRun(
            "INSERT INTO STORAGE_ITEM (storageShelfId, productId, quantity) VALUES (?, ?, ?)",
            [storageItem.storageShelfId, storageItem.productId, storageItem.quantity]
            );
        }
    }    

    await dbRun(`CREATE VIEW product_stock AS
    SELECT 
      p.id, 
      p.productCode, 
      p.productName, 
      IFNULL(SUM(li.quantity), 0) AS total_demandado, 
      IFNULL((SELECT SUM(si.quantity) FROM STORAGE_ITEM si WHERE si.productId = p.id), 0) AS total_disponible
    FROM PRODUCT p
    LEFT JOIN LINE_ITEM li ON p.id = li.productId
    GROUP BY p.id;`);

    rows = await dbAll(`SELECT * from product_stock ps 
        where ps.total_demandado > ps.total_disponible`);

    console.log('Ensure there is enough stock')
    await Promise.all(rows.map(async (line) => {
        let updateId = line.id;
        let diff = line.total_demandado - line.total_disponible;

        await dbRun(`update STORAGE_ITEM set quantity = quantity + ${diff}
        where productId == ${updateId}
        AND storageShelfId IN (
            select storageShelfId from STORAGE_ITEM si 
            where si.productId == ${updateId}
            LIMIT 1
        )`)
    }));

    db.close();
}

program
    .command('db')
    .description('Regenera la base de datos del proyecto 3.')
    .action(async () => { await populateDatabase() });

program
    .command('p1')
    .description('Establece el proyecto 1 como predeterminado.')
    .action(() => { setEnvironment('p1') });

program
    .command('p2')
    .description('Establece el proyecto 2 como predeterminado.')
    .action(() => { setEnvironment('p2') });

program
    .command('p3')
    .description('Establece el proyecto 3 como predeterminado.')
    .action(() => { setEnvironment('p3') });

program
    .command('clean')
    .description('Elimina archivos que pueden generar problemas en el proyecto')
    .action(async () => {

        await Promise.all(
            ['', 
            'ros2_ws/src', 'ros2_ws/src/project1', 
            'project2_ws/src', 'project2_ws/src/project2',
            'project3_ws/src', 'project3_ws/src/project3']
            .map(async (folder) => {
                if (fs.existsSync(path.join(homedir, `${folder}/build`))){
                    const clean_folder = path.join(homedir, folder);
                    console.log("Borrando archivos de colcon en", clean_folder)
                    await exec({ cmd: `bash -c "cd ${clean_folder}; rm -rf build install log"`});
                }
            })
        );

        await Promise.all(
            ['', 
            'ros2_ws', 'ros2_ws/src',
            'project2_ws', 'project2_ws/src',
            'project3_ws', 'project3_ws/src',]
            .map(async (folder) => {
                if (fs.existsSync(path.join(homedir, `${folder}/.git`))){
                    const clean_folder = path.join(homedir, folder);
                    console.log("Borrando repositorio de", clean_folder)
                    await exec({ cmd: `bash -c "cd ${clean_folder}; rm -rf .git"`});
                }
            })
        ); 
        
        await Promise.all(
            ['venv',]
            .map(async (folder) => {
                if (fs.existsSync(path.join(homedir, `${folder}/.git`))){
                    const clean_folder = path.join(homedir, folder);
                    console.log("Borrando de carpeta obsoleta", clean_folder)
                    await exec({ cmd: `bash -c "rm -rf ${clean_folder}"`});
                }
            })
        ); 
    });

program.parse();
